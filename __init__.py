# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import material_request
from . import work
from . import stock

__all__ = ['register']


def register():
    Pool.register(
        material_request.MaterialRequest,
        material_request.MaterialRequestLine,
        material_request.MaterialRequestConfiguration,
        material_request.MaterialRequestConfigurationSequence,
        work.Project,
        stock.ShipmentOut,
        stock.StockMove,
        module='electrans_material_request', type_='model')
    Pool.register(
        module='electrans_material_request', type_='wizard')
    Pool.register(
        module='electrans_material_request', type_='report')
