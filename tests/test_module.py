# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.tests.test_tryton import ModuleTestCase
import trytond.tests.test_tryton
import unittest


class ElectransMaterialRequestTestCase(ModuleTestCase):
    "Test Electrans Material Request module"
    module = 'electrans_material_request'


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        ElectransMaterialRequestTestCase))
    return suite
