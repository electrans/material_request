from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from trytond.pyson import Eval
from trytond.transaction import Transaction
from functools import wraps


def process_material_request_move(func):
    @wraps(func)
    def wrapper(cls, moves):
        pool = Pool()
        MaterialRequest = pool.get('material.request')
        with Transaction().set_context(_check_access=False):
            material_requests = set(m.material_request for m in cls.browse(moves) if m.material_request)
        func(cls, moves)
        if material_requests:
            MaterialRequest.__queue__.process(material_requests)
    return wrapper


class StockMove(metaclass=PoolMeta):
    __name__ = 'stock.move'

    material_request_line = fields.Many2One(
        'material.request.line', "Material Request Line",
        states={'invisible': ~Eval('material_request_line')},
        depends=['material_request_line'])
    material_request = fields.Function(
        fields.Many2One('material.request', 'Material Request'),
        'get_material_request', searcher='search_material_request')

    @classmethod
    def __setup__(cls):
        super(StockMove, cls).__setup__()

    def get_material_request(self, name):
        if self.material_request_line:
            return self.material_request_line.material_request.id

    @classmethod
    def search_material_request(cls, name, clause):
        return [('material_request_line.material_request',) + tuple(clause[1:])]

    @classmethod
    @process_material_request_move
    def delete(cls, moves):
        super(StockMove, cls).delete(moves)

    @fields.depends('origin', 'material_request_line', 'product', '_parent_material_request_line.product')
    def on_change_with_product_uom_category(self, name=None):
        if self.material_request_line and self.product and self.product.default_uom_category:
            return self.product.default_uom_category.id
        category = super(StockMove, self).on_change_with_product_uom_category(
            name=name)
        return category


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    def _sync_outgoing_move(self, template=None):
        move = super(ShipmentOut, self)._sync_outgoing_move(template)
        if template:
            move.material_request_line = template.material_request_line
        return move
