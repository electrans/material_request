from itertools import groupby
from functools import partial
from trytond.pool import Pool
from trytond.model import ModelSQL, ModelView, fields, Workflow, ModelSingleton
from trytond.transaction import Transaction
from trytond.modules.company.model import CompanyValueMixin, CompanyMultiValueMixin
from trytond.ir.attachment import AttachmentCopyMixin
from trytond.pyson import Eval, Bool
from trytond.tools import sortable_values
from trytond.exceptions import UserError
from trytond.i18n import gettext

_REQUEST_STATES = {
    'readonly': Eval('state') != 'draft',
    }
_DEPENDS = ['state']


class MaterialRequest(Workflow, ModelSQL, ModelView, AttachmentCopyMixin):
    "Material Request"
    __name__ = 'material.request'
    _rec_name = 'number'

    number = fields.Char('Number', readonly=True)
    company = fields.Many2One(
        'company.company', 'Company', required=True, select=True,
        states={
            'readonly': (
                (Eval('state') != 'draft')
                | Eval('lines', [0])
                | Eval('party', True)),
            },
        depends=['state'])
    project = fields.Many2One(
        'work.project', "Project", required=True,
        states=_REQUEST_STATES,
        depends=_DEPENDS)
    employee = fields.Many2One(
        'company.employee', "Requester",
        states=_REQUEST_STATES,
        depends=_DEPENDS)
    party = fields.Function(
        fields.Many2One('party.party', "Party"),
        'on_change_with_party')
    shipment_address = fields.Many2One(
        'party.address', "Shipment Address", required=True,
        domain=[('party', '=', Eval('party'))],
        states=_REQUEST_STATES,
        depends=_DEPENDS + ['party'])
    ship_to = fields.Many2One(
        'company.employee', "Ship to",
        states=_REQUEST_STATES,
        depends=_DEPENDS,
        help="The employee who will receive the materials")
    planned_delivery_date = fields.Date(
        'Planned Delivery Date',
        states=_REQUEST_STATES,
        depends=_DEPENDS,
        help="Will be used as planned delivery date for lines if you don't "
             "specify it")
    warehouse = fields.Many2One(
        'stock.location', "Warehouse",
        states=_REQUEST_STATES,
        depends=_DEPENDS)
    lines = fields.One2Many(
        'material.request.line', 'material_request', "Lines",
        states=_REQUEST_STATES,
        depends=_DEPENDS)
    shipments = fields.Function(
        fields.One2Many('stock.shipment.out', None, "Shipments"),
        'get_shipments')
    moves = fields.Function(
        fields.One2Many('stock.move', None, "Moves"),
        'get_moves')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('processing', 'Processing'),
        ('done', 'Done'),
        ('cancelled', 'Cancelled'),
    ], "State",
        readonly=True)
    # Used in Material Request email template
    shipment_numbers = fields.Function(
        fields.Char('Shipment Numbers'), 'get_shipment_numbers')

    @classmethod
    def __setup__(cls):
        super(MaterialRequest, cls).__setup__()
        cls._transitions = set((
            ('draft', 'processing'),
            ('draft', 'cancelled'),
            ('cancelled', 'draft'),
            ('processing', 'done'),
        ))
        cls._buttons.update({
            'cancel': {
                'invisible': Eval('state') != 'draft',
                'depends': ['state'],
            },
            'draft': {
                'invisible': Eval('state') != 'cancelled',
                'depends': ['state'],
            },
            'process': {
                'invisible': ((Eval('state') != 'draft')
                              | ~Eval('lines', [])),
                'readonly': ~Eval('lines', []),
                'depends': ['state'],
            },
            'done': {
                'invisible': Eval('state') != 'processing',
                'depends': ['state'],
            },
        })

    @classmethod
    def create(cls, vlist):
        MaterialRequestConfiguration = Pool().get('material_request.configuration')
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('number'):
                config = MaterialRequestConfiguration(1)
                if not config.material_request_sequence:
                    continue
                values['number'] = config.material_request_sequence.get()
        return super(MaterialRequest, cls).create(vlist)

    @classmethod
    def copy(cls, material_requests, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('planned_delivery_date', None)
        return super(MaterialRequest, cls).copy(material_requests, default=default)

    @fields.depends('project', '_parent_project.party')
    def on_change_with_party(self, name=None):
        return self.project.party.id if self.project else None

    def get_shipments(self, name):
        shipments = set()
        for line in self.lines:
            for move in line.moves:
                shipments.add(move.shipment.id)
        return list(shipments)

    def get_moves(self, name=None):
        moves = set()
        for line in self.lines:
            for move in line.moves:
                moves.add(move.id)
        return list(moves)

    def get_shipment_numbers(self, name=None):
        # Get all shipment numbers from a sale
        return ', '.join(shipment.number for shipment in self.shipments)

    def _group_shipment_key(self, moves, move):
        '''
        The key to group shipments by move planned date

        move is a tuple of line and a move
        '''
        _, move = move

        return (
            [('planned_date', move.planned_date),
             ('warehouse', self.warehouse.id)]
        )

    def create_shipment_out(self):
        '''
        Create and return shipments of type stock.shipment.out
        '''
        pool = Pool()

        moves = self._get_shipment_moves()
        if not moves:
            return

        keyfunc = partial(self._group_shipment_key, list(moves.values()))
        Shipment = pool.get('stock.shipment.out')
        moves = moves.items()
        moves = sorted(moves, key=sortable_values(keyfunc))

        shipments = []
        for key, grouped_moves in groupby(moves, key=keyfunc):
            shipment = self._get_shipment_material_request(Shipment, key)
            shipment.moves = (list(getattr(shipment, 'moves', []))
                + [x[1] for x in grouped_moves])
            shipment.save()
            self.copy_resources_to(shipment)
            shipments.append(shipment)
        Shipment.wait(shipments)
        return shipments

    def _get_shipment_material_request(self, Shipment, key):
        values = {
            'customer': self.party,
            'delivery_address': self.shipment_address,
            'company': Transaction().context['company']
        }
        values.update(dict(key))
        return Shipment(**values)

    def _get_shipment_moves(self):
        moves = {}
        for line in self.lines:
            move = line.create_outgoing_move(self)
            if move:
                moves[line] = move
        return moves

    def check_lines_date_defined(self):
        for line in self.lines:
            if not line.planned_date:
                return False
        return True

    @fields.depends('planned_delivery_date')
    def on_change_with_planned_delivery_date(self, name=None):
        Date = Pool().get('ir.date')
        if self.planned_delivery_date:
            return max(self.planned_delivery_date, Date.today())
        return

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_employee():
        return Transaction().context.get('employee')

    @staticmethod
    def default_warehouse():
        return Pool().get('stock.location').get_default_warehouse()

    @classmethod
    def search_rec_name(cls, name, clause):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, material_requests):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, material_requests):
        pass

    @classmethod
    @Workflow.transition('processing')
    def proceed(cls, material_requests):
        pass

    @classmethod
    @ModelView.button
    def process(cls, material_requests):
        for material_request in material_requests:
            if (not material_request.planned_delivery_date
                    and not material_request.check_lines_date_defined()):
                raise UserError(gettext(
                    'electrans_material_request.no_planned_date_defined'))
            for line in material_request.lines:
                if not line.moves:
                    material_request.create_shipment_out()
        cls.save(material_requests)
        cls.proceed(material_requests)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, material_requests):
        pass


class MaterialRequestLine(Workflow, ModelSQL, ModelView):
    "Material Request Line"
    __name__ = 'material.request.line'

    product = fields.Many2One(
        'product.product', "Product", required=True,
        states=_REQUEST_STATES,
        depends=_DEPENDS)
    material_request = fields.Many2One(
        'material.request', "Material Request",
        readonly=True)
    description = fields.Text('Description',
        states=_REQUEST_STATES,
        depends=_DEPENDS)
    uom = fields.Many2One(
        'product.uom', "Uom",
        states={
            'required': Bool(Eval('product')),
            'readonly': Eval('state') != 'draft'
        },
        depends=['product', 'state'])
    quantity = fields.Integer(
        'Quantity',
        states={
            'required': Bool(Eval('product')),
            'readonly': Eval('state') != 'draft'
        },
        depends=['product', 'state'])
    shipping_date = fields.Function(
        fields.Date('Shipping Date'),
        'on_change_with_shipping_date')
    planned_date = fields.Date(
        'Planned Date',
        states=_REQUEST_STATES,
        depends=_DEPENDS)
    sale_line = fields.Many2One(
        'sale.line', "Sale Line",
        domain=[('sale.is_order', '=', True),
                ('sale.not_from_presupuestario', '=', True),
                ('sale.project', '=', Eval('_parent_material_request', {}).get('project'))],
        required=True
    )
    state = fields.Function(
        fields.Selection('get_request_states', "Request State"),
        'get_request_state')
    moves = fields.One2Many('stock.move', 'material_request_line', 'Moves', readonly=True)
    company = fields.Function(
        fields.Many2One('company.company', "Company"),
        'on_change_with_company')

    @classmethod
    def __setup__(cls):
        super(MaterialRequestLine, cls).__setup__()

    @classmethod
    def copy(cls, lines, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('moves', None)
        default.setdefault('planned_date', None)
        return super(MaterialRequestLine, cls).copy(lines, default=default)

    def get_rec_name(self, name):
        pool = Pool()
        Lang = pool.get('ir.lang')
        if self.product:
            lang = Lang.get()
            return (lang.format(
                    '%.*f', (self.uom.digits, self.quantity or 0))
                + '%s %s @ %s' % (
                    self.uom.symbol, self.product.rec_name,
                    self.material_request.rec_name))
        else:
            return self.material_request.rec_name

    @fields.depends('product', 'uom')
    def on_change_product(self):
        if self.product:
            self.uom = self.product.template.default_uom

    @fields.depends('moves', 'material_request',
        '_parent_material_request.planned_delivery_date')
    def on_change_with_shipping_date(self, name=None):
        moves = [m for m in self.moves if m.state != 'cancelled']
        if moves:
            dates = filter(
                None, (m.effective_date or m.planned_date for m in moves))
            return min(dates, default=None)
        return

    @fields.depends('planned_date', 'material_request',
        '_parent_material_request.planned_delivery_date')
    def on_change_with_planned_date(self):
        today = Pool().get('ir.date').today()
        date = None
        if self.planned_date:
            date = max(self.planned_date, today)
        elif (self.material_request
                and self.material_request.planned_delivery_date):
            date = max(self.material_request.planned_delivery_date, today)
        return date

    @fields.depends('material_request', '_parent_material_request.company')
    def on_change_with_company(self, name=None):
        if self.material_request and self.material_request.company:
            return self.material_request.company.id

    @staticmethod
    def default_state():
        return 'draft'

    def get_request_state(self, name=None):
        return self.material_request.state if self.material_request else 'draft'

    @classmethod
    def get_request_states(cls):
        MaterialRequest = Pool().get('material.request')
        return MaterialRequest.fields_get(['state'])['state']['selection']

    def create_outgoing_move(self, material_request):
        pool = Pool()
        Move = pool.get('stock.move')
        move = Move()
        move.quantity = self.quantity
        move.uom = self.uom
        move.product = self.product
        move.from_location = material_request.warehouse.output_location.id if material_request.warehouse and \
            material_request.warehouse.output_location else None
        move.to_location = material_request.party.customer_location.id if \
            material_request.party.customer_location else None
        move.state = 'draft'
        move.origin = self.sale_line
        move.material_request_line = self.id
        # TODO: the move cost should be the same as the incoming move because the product/lot cost do no change while a reparation is done
        move.unit_price = self.product.cost_price
        move.company = material_request.company
        move.currency = material_request.company.currency
        move.planned_date = (self.planned_date
            or material_request.planned_delivery_date)
        move.on_change_product()
        move.save()
        return move


def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()

    return default


class MaterialRequestConfiguration(ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Material Request Configuration'
    __name__ = 'material_request.configuration'

    material_request_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Material Request Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('context', {}).get('company', -1), None])
        ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'material_request_sequence':
            return pool.get('material_request.configuration.sequence')
        return super(MaterialRequestConfiguration, cls).multivalue_model(field)

    default_material_request_sequence = default_func('material_request_sequence')


class MaterialRequestConfigurationSequence(ModelSQL, CompanyValueMixin):
    "Material Request Configuration Sequence"
    __name__ = 'material_request.configuration.sequence'

    material_request_sequence = fields.Many2One(
        'ir.sequence', "Material Request Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None])
        ],
        depends=['company'])

    @classmethod
    def default_material_request_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('electrans_material_request', 'sequence_material_request')
        except KeyError:
            return None
