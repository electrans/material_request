from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval


class Project(metaclass=PoolMeta):
    __name__ = 'work.project'

    material_requests = fields.One2Many(
        'material.request', 'project', "Material Requests")

    @classmethod
    def __setup__(cls):
        super(Project, cls).__setup__()
